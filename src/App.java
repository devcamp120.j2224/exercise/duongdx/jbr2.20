public class App {
    public static void main(String[] args) throws Exception {
        
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2);
        Circle circle3 = new Circle(4, "green");
   
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);

        System.out.println("Dien Tich Cua 3 Hinh");
        System.out.println("Dien Tich Cua hinh 1:  " + circle1.getArea());
        System.out.println("Dien Tich Cua hinh 2: " +circle2.getArea());
        System.out.println("Dien Tich Cua hinh 3:" +circle3.getArea());

        //  doi tuong Cylinder
        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, "green");
        Cylinder cylinder4 = new Cylinder(3.5, "red", 1.5);

        System.out.println(" hinh 1  " + cylinder1);
        System.out.println(" hinh 2  " + cylinder2);
        System.out.println(" hinh 3  " + cylinder3);
        System.out.println(" hinh 4  " + cylinder4);

        System.out.println("the tich hinh 1 = :  " + cylinder1.getVolume());
        System.out.println("the tich hinh 2 = :  " + cylinder2.getVolume());
        System.out.println("the tich hinh 3 = :  " + cylinder3.getVolume());
        System.out.println("the tich hinh 4 = :  " + cylinder4.getVolume());
    }
}
